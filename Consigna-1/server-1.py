import socket

def main():
    host = '127.0.0.1'
    port = 12345

    
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(1)

    print("Esperando a que un cliente se conecte...")

    
    client_socket, client_address = server_socket.accept()
    print(f"Cliente conectado desde {client_address}")

    
    server_info = f"Servidor: {host}:{port}"
    client_socket.send(server_info.encode())

    client_data = client_socket.recv(1024)
    print(f"Datos del cliente: {client_data.decode()}")

    client_socket.close()

   
    server_socket.close()

if __name__ == "__main__":
    main()
