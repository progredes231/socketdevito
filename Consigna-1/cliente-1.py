import socket

def main():
   
    server_host = '127.0.0.1'
    server_port = 12345

    
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    
    client_socket.connect((server_host, server_port))

    
    server_data = client_socket.recv(1024)
    print(f"Datos del servidor: {server_data.decode()}")

    
    client_info = f"Cliente: {client_socket.getsockname()}"
    client_socket.send(client_info.encode())

    client_socket.close()

if __name__ == "__main__":
    main()
