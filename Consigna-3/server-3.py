import socket
import threading

# Lista para almacenar las conexiones de los clientes
clientes = []

def handle_client(client_socket):
    while True:
        try:
            client_message = client_socket.recv(1024).decode()
            if not client_message:
                break
            print(f"Cliente {client_socket.getpeername()} dice: {client_message}")

            if client_message == "exit":
                break

            for client in clientes:
                if client != client_socket:
                    client.send(f"Cliente {client_socket.getpeername()} dice: {client_message}".encode())
        except Exception as e:
            print(f"Error: {e}")
            break

    
    client_socket.close()
    clientes.remove(client_socket)

def main():
    
    host = '0.0.0.0'
    port = 12345

    
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(5)

    print("Servidor escuchando en {}:{}".format(host, port))

    while True:
       
        client_socket, client_address = server_socket.accept()
        print(f"Cliente {client_address} conectado")

       
        clientes.append(client_socket)

        client_thread = threading.Thread(target=handle_client, args=(client_socket,))
        client_thread.start()

if __name__ == "__main__":
    main()