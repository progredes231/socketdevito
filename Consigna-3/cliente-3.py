import socket

def main():
    
    server_host = '127.0.0.1'
    server_port = 12345

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    
    client_socket.connect((server_host, server_port))

    while True:
       
        client_message = input("vos: ")
        client_socket.send(client_message.encode())

        if client_message == "exit":
            break

       
        server_message = client_socket.recv(1024).decode()
        print(server_message)

        if server_message == "exit":
            break

    
    client_socket.close()

if __name__ == "__main__":
    main()

